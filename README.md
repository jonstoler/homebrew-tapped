# jonstoler's Homebrew Tap

## Usage

`brew tap jonstoler/tapped https://gitlab.com/jonstoler/homebrew-tapped.git`

## Included Software

### Apps (`brew cask`)
* [AlphaSync](http://tsoniq.com/software/legacy/alphasync/): userland driver for [Alphasmart Neo](http://en.wikipedia.org/wiki/AlphaSmart)
* [Apple Juice](https://github.com/raphaelhanneken/apple-juice): battery gauge
* [Ember](https://kb.realmacsoftware.com/hc/en-us/articles/115001205634-Download-Ember-for-Mac): image/inspiration organizer
* [Marta](https://marta.yanex.org/): Native, extensible, fast File Manager for macOS
* [MenuMate](http://zipzapmac.com/MenuMate): App menus where you need them
* [PCalc 3](http://pcalc.com/): non-MAS version of the best calculator for Mac
* [Snk](https://www.mowglii.com/snk/): small snake game
* [SuperGetInfo](http://www.barebones.com/support/super/index.html): Get Info on steroids
* [XMenu](http://www.devontechnologies.com/products/freeware/): Access applications, files, folders, and snippets from the menu bar
