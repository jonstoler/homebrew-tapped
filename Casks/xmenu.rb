cask 'xmenu' do
	version '1.9.8'
  sha256 '225aed0405b40ea7eff97fffa869ad0ad3e163ad1f897e9c3aa1c0afad9cf0c4'

  url "https://s3.amazonaws.com/DTWebsiteSupport/download/freeware/xmenu/#{version}/XMenu.app.zip"
  name 'XMenu'
  homepage 'http://www.devontechnologies.com/products/freeware/'

  app 'XMenu.app'
end
