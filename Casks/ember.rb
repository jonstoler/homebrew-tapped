cask 'ember' do
  version '1.8.5'
  sha256 '3c9bb3be21f2601f47d783c532bd091979628c823ac5690b645e349c4855b794'

  url "https://realmacsoftware.com/downloads/ember-#{version.gsub('.', '')}.zip"
  name 'Ember'
  homepage 'https://www.realmacsoftware.com/ember/'

  app "Ember #{version}.app"
end
