cask 'snk' do
  version '1.3.1'
  sha256 '69183835011d4ab32a703818a80d4554abf6598933a7d883298b465c32981621'

  url 'https://s3.amazonaws.com/mowglii/Snk.zip'
  name 'Snk'
  homepage 'https://www.mowglii.com/snk/'

  app "Snk.app"
end
