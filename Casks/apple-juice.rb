cask 'apple-juice' do
  version '1.2.2'
  sha256 'a20cb53cf0903ccb504ac580149ae6d9f5263650a5710596c63624bcf30e5b49'

  url "https://github.com/raphaelhanneken/apple-juice/releases/download/#{version}/Apple.Juice.dmg"
  name 'Apple Juice'
  homepage 'https://github.com/raphaelhanneken/apple-juice'

  app 'Apple Juice.app'
end
