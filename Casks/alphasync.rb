cask 'alphasync' do
  version '1.3'
  sha256 '4af9b04bc7ce1ed846075caaedcd6b271c4b7bc5b67fcbeb0639a199d221d435'

  url 'http://tsoniq.com/software/legacy/alphasync/alphasync13.dmg'
  name 'AlphaSync'
  homepage 'http://tsoniq.com/software/legacy/alphasync/'

  app 'AlphaSync.app'
end
