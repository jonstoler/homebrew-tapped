cask 'menumate' do
  version '1.3'
  sha256 'f037edbe8dbf47264f1e6de0b3753d8c0829f4fdcd275bc4bca196d8e23e7025'

  url 'http://zipzapmac.com/DMGs/MenuMate.dmg'
  name 'MenuMate'
  homepage 'http://zipzapmac.com/MenuMate'

  app "MenuMate.app"
end
