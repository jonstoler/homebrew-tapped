cask 'marta' do
  version '0.4.3'
  sha256 '4412e88b45c42bdc3e328493d1f7d43ace30496a9be28da345a0d39bf4528d74'

  url 'https://marta.yanex.org/updates/Marta-0.4.3.dmg'
  name 'Marta'
  homepage 'https://marta.yanex.org/'

  app 'Marta.app'
end
