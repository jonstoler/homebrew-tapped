cask 'supergetinfo' do
  version '1.3.1'
  sha256 'f50cb6c3623b035128dec4716839c07a36e7d50b199d5288946bb12b1ad30574'

  url 'http://pine.barebones.com/files/SuperGetInfo.dmg'
  name 'Super Get Info'
  homepage 'http://barebones.com'

  app "Super Get Info.app"
end
